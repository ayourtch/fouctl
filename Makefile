fouctl: fouctl.c
	 gcc -o fouctl -g fouctl.c -lcrypto
install: fouctl
	cp fouctl /usr/local/bin
	cp fouctl-update-cmd /usr/local/bin
clean:
	rm -f fouctl
