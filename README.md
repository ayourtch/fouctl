# README #

### What is this ? ###

* A very lightweight way to update [FOU](https://lwn.net/Articles/614348/) tunnels from roaming clients behind the NATs


### How do I get set up? ###

* git clone https://bitbucket.org/ayourtch/fouctl
* make; sudo make install 

Then, for the server setup, look at serverside-init file: the operations there need to be done once, upon the server startup/provisioning of the tunnel.

The client stuff is in client/* - the default settings as well as the "keepalive" script. As you see, other than "sha256sum", there is no need for additional software
on the client, which is handy.

The tunneled traffic is going over the internet in clear text, keep this in mind!

