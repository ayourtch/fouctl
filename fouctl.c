/*
 * A small daemon to maintain the Foo over UDP tunnels on the server side 
 * for the roaming / NATted clients.
 * 
 * Clients periodically send keepalives, which are IPv6 ping packets with the payload being:
 * - First two bytes: tunnel number.
 * - The next 14 bytes: the first 14 bytes of SHA256 hash of <time>+<secret> string.
 * 
 * The program takes two arguments: the UDP port for FOU server and the filename with the secrets.
 * Each line in the secrets file has the tunnel number followed by the secret.
 *
 * Copyright (c) 2016 Andrew Yourtchenko <ayourtch@gmail.com>
 * License: MIT.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/socket.h>
#include <errno.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <time.h>
#include <openssl/sha.h>
#include <ctype.h>

#define DGRAM_MAX 16384

#define debug printf
// #define debug(...)

uint16_t udp_port;
char *secrets_file;
char *update_command;

uint8_t datagram[DGRAM_MAX];

void dump(void *data, int len) {
  uint8_t *p = data;
  int count = 0;
  for(; len > 0; len--, count++, p++) {
    if(0 == count % 16) {
      printf("\n%p:", p);
    }
    printf(" %02x", *p);
  }
  printf("\n");
}

char *secrets[65536];

void readsecrets(char *fname) {
  memset(secrets, sizeof(secrets), 0);
  FILE *f = fopen(fname, "r");
  char line[1024];
  while (f && fgets(line, sizeof(line)-1, f)) {
    char *tunnel = line;
    char *secret = line;
    char *nl;
    while(*secret && !isspace(*secret)) { secret++; }
    if(*secret) {
      *secret++ = 0;
      while(*secret && isspace(*secret)) { secret++; }
      nl = secret;
      while(*nl && (*nl != '\n')) { nl++; }
      *nl = 0;
      
    }
    {
      long tun = strtol(tunnel, NULL, 0);
      if ((tun >= 0) && (tun <= 65535)) {
	secrets[tun] = malloc(strlen(secret)+1);
	if (secrets[tun]) {
	  memcpy(secrets[tun], secret, strlen(secret)+1);
	}
      }
      
    }
  }
  if(f) {
    fclose(f);
  }
}

int main(int argc, char *argv[]) {
  int one = 1;
  int s = socket (PF_INET, SOCK_RAW, IPPROTO_UDP);
  if (argc < 4) {
    printf("Usage: %s <udp_port> <secrets file> <update command>\n", argv[0]);
    exit(-42);
  }
  
  if (s < 0) {
    perror("Creating raw socket failed");
    exit(-42);
  }
  if (setsockopt (s, IPPROTO_IP, IP_HDRINCL, &one, sizeof(one)) < 0) {
    perror("setsockopt failed");
    exit(-42);
  }

  udp_port = strtol(argv[1], NULL, 0);
  readsecrets(argv[2]);
  update_command = argv[3];

  while (1) { 
    struct sockaddr_in peer_addr;
    struct sockaddr_in peer_addr_iphdr;
    struct sockaddr_in my_addr_iphdr;
    socklen_t peer_addr_len = sizeof(peer_addr);
    struct iphdr *ip = (void *)datagram;
    struct udphdr *udp;

    int n = recvfrom(s, datagram, sizeof(datagram), 0, (struct sockaddr *)&peer_addr, &peer_addr_len);
    debug("Recvfrom: %d from %s:%d\n", n, inet_ntoa(peer_addr.sin_addr), ntohs(peer_addr.sin_port));
    debug("Totlen: %d %d\n", ntohs(ip->tot_len), n);
    if((4 != ip->version) || (IPPROTO_UDP != ip->protocol) || (ntohs(ip->tot_len) + 8 != n) || (ip->ihl != 5)) {
      continue;
    }
    peer_addr_iphdr.sin_addr.s_addr = ip->saddr;
    my_addr_iphdr.sin_addr.s_addr = ip->daddr;
    debug("Source in IP header: %s\n", inet_ntoa(peer_addr_iphdr.sin_addr));
    if (peer_addr_iphdr.sin_addr.s_addr != peer_addr.sin_addr.s_addr) {
      continue;
    }
    udp = (void*)(ip+1);
    debug("UDP sport: %u dport: %u, len: %u\n", ntohs(udp->source), ntohs(udp->dest), ntohs(udp->len));
    if (ntohs(udp->len) + 12 != ntohs(ip->tot_len)) {
      continue;
    }
    if (ntohs(udp->dest) != udp_port) { 
      continue;
    }
    {
      uint16_t *tunnel_nr_net = (void *)&datagram[92];
      uint16_t tunnel_nr = ntohs(*tunnel_nr_net);
      debug("Tunnel #: %04x\n", tunnel_nr);
      if (secrets[tunnel_nr]) {
	uint8_t hash[SHA256_DIGEST_LENGTH];
	uint8_t *phash_pkt = &datagram[94];
	SHA256_CTX sha256;
	char buf[1024];
	time_t t = time(NULL);
	snprintf(buf, sizeof(buf)-1, "%ld+%s", t, secrets[tunnel_nr]);
	SHA256_Init(&sha256);
	SHA256_Update(&sha256, buf, strlen(buf));
	SHA256_Final(hash, &sha256);
	if(0 == memcmp(hash, phash_pkt, 14)) {
	  char cmd[1024];
	  snprintf(cmd, sizeof(cmd)-1, "%s %04x %s %d %d",  
		       update_command, tunnel_nr, inet_ntoa(peer_addr_iphdr.sin_addr), ntohs(udp->dest), ntohs(udp->source));
	  debug("Match!\n");
	  debug("cmd: %s\n", cmd);
	  system(cmd);
	}
      } else {
	debug("No secret for tunnel %04x\n", tunnel_nr);
      }
    }
    
  }
}

